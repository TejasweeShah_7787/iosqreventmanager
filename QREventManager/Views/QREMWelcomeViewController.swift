//
//  QREMScanViewController.swift
//  QREventManager
//
//  Created by Hannes Töllborg on 15/02/16.
//  Copyright © 2016 Hannes Töllborg. All rights reserved.
//

import UIKit

class QREMWelcomeViewController: UIViewController {
    
    @IBOutlet weak var loggedInLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        loggedInLabel.text = Requester.sharedInstance.eventUser!.eventId
        
        //Nav title in this view
        self.title = "Scan"
    }
    
    //Log out action
    @IBAction func logoutAction(sender: UIButton) {
        NSNotificationCenter.defaultCenter().postNotificationName(LOGOUTNOTIFICATION, object: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
