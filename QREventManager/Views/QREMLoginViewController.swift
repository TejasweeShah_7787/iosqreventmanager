//
//  QREMLoginViewController.swift
//  QREventManager
//
//  Created by Hannes Töllborg on 15/02/16.
//  Copyright © 2016 Hannes Töllborg. All rights reserved.
//
import Foundation
import UIKit

class QREMLoginViewController: UIViewController {
    
    @IBOutlet weak var eventName: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    private var keyboardIsShowing = false
    
    private var oldBottomConstant: CGFloat?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        var imageView = UIImageView(image: UIImage(named: "login"))
        imageView.contentMode = .Right
        eventName.leftView = imageView
        eventName.leftView!.frame = CGRectMake(10, 0, 20, 20)
        eventName.leftViewMode = .Always
        
        imageView = UIImageView(image: UIImage(named: "password"))
        imageView.contentMode = .Right
        passwordField.leftView = imageView
        passwordField.leftView!.frame = CGRectMake(10, 0, 20, 20)
        passwordField.leftViewMode = .Always
        
        // aciton for login button
        loginButton.addTarget(self, action: "login:", forControlEvents: .TouchUpInside)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShowNotification:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHideNotification:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    // MARK: - Notifications
    func keyboardWillShowNotification(notification: NSNotification) {
        keyboardIsShowing = true
    }
    
    func keyboardWillHideNotification(notification: NSNotification) {
        keyboardIsShowing = false
    }
    
}

//Login extension
extension QREMLoginViewController {
    
    ///  Handles login prompt for login button
    ///
    ///  - parameter sender: the button itself
    func login(sender:UIButton?){
        
        let eventUser = EventUser(eventId: eventName.text!, passwd: passwordField.text!)
        Requester.sharedInstance.eventUser = eventUser
        
        self.view.endEditing(true)
        
        self.loginButton.enabled = false
        
        //do get request with our user
        //validate response
        if NETWORKENABLED {
            Requester.sharedInstance.PostRequest(NetworkConfig.BASEURL + "/login", parameters: nil)!.responseJSON {
                response in
                //print(response.response?.statusCode);
                guard let _response = response.response else {
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.notification.displayNotificationWithMessage("Could not connect to the server!", forDuration: 2.0)
                    Requester.sharedInstance.eventUser = nil
                    self.loginButton.enabled = true
                    return
                }

                switch _response.statusCode {
                case STATUS_CODES.OK.rawValue:
                    // Move to next view
                    // Change the root view controller
                    AppDelegate.sharedAppDelegate()?.changeRootViewControllerWithIdentifier("RootViewNavigationController")
                    break
                case STATUS_CODES.INCORRECT.rawValue:
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.notification.displayNotificationWithMessage("Wrong credientials, please try again!", forDuration: 2.0)
                    //null eventuser
                    Requester.sharedInstance.eventUser = nil
                    self.loginButton.enabled = true
                    break
                case STATUS_CODES.NOMATCH.rawValue:
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.notification.displayNotificationWithMessage("No matching credentials!", forDuration: 2.0)
                    //null eventuser
                    Requester.sharedInstance.eventUser = nil
                    self.loginButton.enabled = true
                    break
                default:
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.notification.displayNotificationWithMessage("An internal server error occurred.", forDuration: 2.0)
                    Requester.sharedInstance.eventUser = nil
                    self.loginButton.enabled = true
                    break
                    
                }
            }
            
        } else {
            // Move to next view
            // Change the root view controller
            AppDelegate.sharedAppDelegate()?.changeRootViewControllerWithIdentifier("RootViewNavigationController")
        }
    }
}
