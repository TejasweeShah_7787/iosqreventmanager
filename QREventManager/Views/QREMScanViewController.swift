//
//  QREMScanViewController.swift
//  QREventManager
//
//  Created by Hannes Töllborg on 15/02/16.
//  Copyright © 2016 Hannes Töllborg. All rights reserved.
//

import UIKit
import AVFoundation
import NYAlertViewController
import AudioToolbox

class QREMScanViewController: UIViewController {
    
    enum DIALOG_STATE: String {
        case UPDATING = "Updating"
        case SUCCESS = "Success"
        case EXISTS = "Exists"
        case ERROR = "Error"
    }
    
    
    @IBOutlet weak var simulatorButton: UIButton!
    @IBOutlet weak var simulatorLabel: UILabel!
    @IBOutlet weak var simulatorTextField: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var scannerLabel: UILabel!
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    
    var qrCodeFrameView:UIView?
    
    var dialogShown = false
    var scanInProgress = false
    var alertViewController:NYAlertViewController?
    var timer:NSTimer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Scan a QR-tag"
        
#if (arch(i386) || arch(x86_64)) && os(iOS)
        // If simulator (no camera supprt), just add manual entry of guestId via textField
    simulatorTextField.hidden = false
    simulatorLabel.hidden = false
    simulatorButton.hidden = false
    
    simulatorButton.addTarget(self, action: "simulatorButtonPress", forControlEvents: .TouchUpInside)
    
#else
        
        //get instance of AVCaptureDevice class in order to initialize a device object and provide video
        //as media type parameter
        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        //get instance of AVCaptureDeviceInput class using prev. device object
        var input: AnyObject?
        
        do {
            input = try AVCaptureDeviceInput(device:captureDevice) as AVCaptureDeviceInput
        } catch let error as NSError {
            //if error occurs, print the description of the error
            print(error.localizedDescription)
        }
        
        //Initialize captureSession object
        captureSession = AVCaptureSession()
        //Set the input devuce on the caputre session
        captureSession?.addInput(input as? AVCaptureInput)
        
        // Initialize an AVCaptureMetadataOutput object and set it as the output device to the capture session
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        
        //Set delegate and use default dispatch queue to execute the callback
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue(captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]))
        
        //Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        videoPreviewLayer?.frame = view.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        //Start video capture
        captureSession?.startRunning()
        
        view.bringSubviewToFront(scannerLabel);
        
        //Create QR code highlight square
        //Initialize QR code frame to highlight qr code
        qrCodeFrameView = UIView()
        qrCodeFrameView?.layer.borderColor = UIColor.greenColor().CGColor
        qrCodeFrameView?.layer.borderWidth = 2
        view.addSubview(qrCodeFrameView!)
        view.bringSubviewToFront(qrCodeFrameView!)
        
        updateRotation()
#endif
    }
    
    
    func simulatorButtonPress() {
        self.processQrCode(simulatorTextField.text!)
    }
    
    //animate while rotating, camera works along and gets new view frame size
    @available(iOS 8, *)
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        coordinator.animateAlongsideTransition({ (UIViewControllerTransitionCoordinatorContext) -> Void in
            self.videoPreviewLayer?.frame = self.view.bounds
            self.updateRotation()
            }, completion: nil)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
                updateRotation()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissAlert(){
        self.alertViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.dialogShown = false
        })
        
    }
    
    func updateRotation(){
        
        if let connection =  self.videoPreviewLayer?.connection  {
            //let currentDevice: UIDevice = UIDevice.currentDevice()
            //let orientation: UIDeviceOrientation = currentDevice.orientation
            let orientation = UIApplication.sharedApplication().statusBarOrientation
            
            //let previewLayerConnection : AVCaptureConnection = connection
            
            if (connection.supportsVideoOrientation)
            {
                switch (orientation)
                {
                case .Portrait:
                    connection.videoOrientation = AVCaptureVideoOrientation.Portrait
                    break
                case .LandscapeRight:
                    connection.videoOrientation = AVCaptureVideoOrientation.LandscapeRight
                    break
                case .LandscapeLeft:
                    connection.videoOrientation = AVCaptureVideoOrientation.LandscapeLeft
                    break
                case .PortraitUpsideDown:
                    connection.videoOrientation = AVCaptureVideoOrientation.PortraitUpsideDown
                    break
                    
                    
                default:
                    connection.videoOrientation = AVCaptureVideoOrientation.Portrait
                    break
                }
            }
        }
        
    }
    
    
    func showDialogWithState(state:DIALOG_STATE, responseText:String?, statusCode:Int?){
        
        alertViewController = NYAlertViewController()
        //image containter
        let imageView = UIImageView()
        imageView.contentMode = .ScaleAspectFit
        var image = UIImage()
        
        
        if state == DIALOG_STATE.SUCCESS {
            image = UIImage(named: "success")!
            //textView.text = "User successfully signed in!"
        } else if state == DIALOG_STATE.ERROR {
            image = UIImage(named: "error")!
            //textView.text = "User is not listed!"
        } else if state == DIALOG_STATE.EXISTS {
            image = UIImage(named:"exists")!
        }
        
        //add image to image view
        imageView.image = image
        
        imageView.translatesAutoresizingMaskIntoConstraints = false // disable automatic constraint generation
        
        //assign image to alertViewContentView
        alertViewController?.alertViewContentView = imageView
        
        // pressing ok in error dialog
        let cancelAction = NYAlertAction(
            title: "Ok",
            style: .Cancel,
            handler: { (action: NYAlertAction!) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
                self.dialogShown = false
            }
        )
        
        switch state {
        case .SUCCESS:
            alertViewController!.title = "QR Accepted"
            alertViewController!.message = ""
            //timer to dismiss dialog after 2.0 seconds
            self.presentViewController(alertViewController!, animated: true, completion: { () -> Void in
                self.timer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: "dismissAlert", userInfo: nil, repeats: false)
            })
            //alertViewController!.view.tintColor = UIColor.redColor()
            break;
        case .ERROR:
            alertViewController!.title = DIALOG_STATE.ERROR.rawValue
            alertViewController?.addAction(cancelAction)
            switch statusCode! {
            case STATUS_CODES.INCORRECT.rawValue:
                alertViewController!.message = "Incorrect QR Credentials!"
                self.presentViewController(alertViewController!, animated: true, completion: {() -> Void in
                })
                break
            case STATUS_CODES.NOMATCH.rawValue:
                alertViewController!.message = "Wrong QR!"
                self.presentViewController(alertViewController!, animated: true, completion: {() -> Void in
                })
                break
            case STATUS_CODES.UNKNOWN.rawValue:
                alertViewController!.message = "Guest does not exist for this event!"
                self.presentViewController(alertViewController!, animated: true, completion: {() -> Void in
                })
                break
            case -999:
                alertViewController!.message = "Server is down!!"
                self.presentViewController(alertViewController!, animated: true, completion: {() -> Void in
                })
                break
            default:
                alertViewController!.message = "A server error occurred!" + " (status code: " + String(statusCode!) + ")"
                self.presentViewController(alertViewController!, animated: true, completion: {() -> Void in
                })
                break
            }
        case .EXISTS:
            alertViewController!.title = "QR Already Scanned!"
            alertViewController!.message = ""
            alertViewController?.addAction(cancelAction)
            self.presentViewController(alertViewController!, animated: true, completion: {() -> Void in
            })
            
            break;
        case .UPDATING:
            alertViewController!.message = ""
            alertViewController!.title = DIALOG_STATE.UPDATING.rawValue
            break;
            
        }
        
        self.dialogShown = true
    }
    
    func processQrCode(code:String) {
        if(!dialogShown && !scanInProgress) {
            scanInProgress = true
            spinner.hidden = false
            spinner.startAnimating()
            
            //scannerLabel.hidden = true
            
            // create post object
            let qrDict:[String:String] = ["eventId":(Requester.sharedInstance.eventUser?.eventId)!, "guestId":code]
            
            if NETWORKENABLED {
                Requester.sharedInstance.PostRequest(NetworkConfig.BASEURL + "/scan", parameters: qrDict)?.responseJSON {
                    response in
                    
                    self.scanInProgress = false
                    
                    guard let _response = response.response else {
                        self.spinner.stopAnimating()
                        self.spinner.hidden = true
                        self.showDialogWithState(DIALOG_STATE.ERROR, responseText: nil, statusCode: -999)
                        return
                    }
                    
                    self.spinner.stopAnimating()
                    self.spinner.hidden = true
                    
                    switch _response.statusCode {
                    case STATUS_CODES.OK.rawValue:
                        self.showDialogWithState(DIALOG_STATE.SUCCESS, responseText: nil, statusCode: _response.statusCode)
                        break
                    case STATUS_CODES.UNKNOWN.rawValue:
                        self.showDialogWithState(DIALOG_STATE.ERROR, responseText: nil, statusCode: _response.statusCode)
                        break
                    case STATUS_CODES.CONFLICT.rawValue:
                        self.showDialogWithState(DIALOG_STATE.EXISTS, responseText: nil, statusCode: _response.statusCode)
                        break
                    default:
                        self.showDialogWithState(DIALOG_STATE.ERROR, responseText: nil, statusCode: _response.statusCode)
                        break
                    }
                    
                }
                
            } else { // demo
                //below switch for demo purpose
                
                self.scanInProgress = false
                
                switch code {
                case "200":
                    
                    self.spinner.stopAnimating()
                    self.spinner.hidden = true
                    self.showDialogWithState(DIALOG_STATE.SUCCESS, responseText: nil, statusCode: 200)
                    break
                case "208":
                    
                    self.spinner.stopAnimating()
                    self.spinner.hidden = true
                    self.showDialogWithState(DIALOG_STATE.EXISTS, responseText: nil, statusCode: 208)
                    break
                case "400":

                    self.spinner.stopAnimating()
                    self.spinner.hidden = true
                    self.showDialogWithState(DIALOG_STATE.ERROR, responseText: nil, statusCode: 400)
                    break
                case "401":
                    
                    self.spinner.stopAnimating()
                    self.spinner.hidden = true
                    self.showDialogWithState(DIALOG_STATE.ERROR, responseText: nil, statusCode: 401)
                    break
                case "403":
                    
                    self.spinner.stopAnimating()
                    self.spinner.hidden = true
                    self.showDialogWithState(DIALOG_STATE.ERROR, responseText: nil, statusCode: 403)
                    break
                default:
                    
                    self.spinner.stopAnimating()
                    self.spinner.hidden = true
                    if let _code = Int(code) {
                        self.showDialogWithState(DIALOG_STATE.ERROR, responseText: nil, statusCode: _code)
                    } else {
                        self.showDialogWithState(DIALOG_STATE.ERROR, responseText: nil, statusCode: 400)
                    }
                    break
                    
                }
            }
        }
    }
}


// MARK: - AVCaptureMetadataOutputObjectsDelegate
extension QREMScanViewController:AVCaptureMetadataOutputObjectsDelegate {
    
    ///  Capture QR code metadata
    ///
    ///  - parameter captureOutput:   output type
    ///  - parameter metadataObjects: array containin metadata objects
    ///  - parameter connection:      connection to capture camera
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        //Check if the metadataObjects array is not nil and it contains at least one object
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRectZero
            scannerLabel.text = "No QR code detected"
            //scannerLabel.tintColor = UIColor.whiteColor() -- not working!?
            return
        }
        
        //Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        //found a metadata object of correct type
        if metadataObj.type == AVMetadataObjectTypeQRCode {
            
            // Vibrate
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            
            //if the found metadata is equal to the QR code metadata, then update the status label's text and set bounds
            // for highliting barcode
            let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            qrCodeFrameView?.frame = barCodeObject.bounds
            
            //if contains value, present it to scannerlabel
            if let qrCode = metadataObj.stringValue {
                self.processQrCode(qrCode)
            }
        }
    }
}
