//
//  AppDelegate.swift
//  QREventManager
//
//  Created by Hannes Töllborg on 15/02/16.
//  Copyright © 2016 Hannes Töllborg. All rights reserved.
//

import UIKit
import Foundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let notification = CWStatusBarNotification()
    
    var window: UIWindow?
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        UINavigationBar.appearance().barStyle = .Black
        
        // Override point for customization after application launch.
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().barTintColor = CCPRIMARY
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        let backButtonImage = UIImage(named: "backbutton")
        //backButtonImage = backButtonImage?.imageWithRenderingMode(.AlwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = backButtonImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backButtonImage
        
        if #available(iOS 9.0, *) {
            UIBarButtonItem.appearanceWhenContainedInInstancesOfClasses([UINavigationBar.self]).tintColor = UIColor.whiteColor()
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 9.0, *) {
            UIBarButtonItem.appearanceWhenContainedInInstancesOfClasses([UINavigationBar.self]).setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), forBarMetrics: UIBarMetrics.Default)
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 9.0, *) {
            UIBarButtonItem.appearanceWhenContainedInInstancesOfClasses([UINavigationBar.self]).setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), forBarMetrics: UIBarMetrics.Compact)
        } else {
            // Fallback on earlier versions
        }

        
        // Add a KVO observer so we know if a post has been updated
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "logoutNotificationTriggered", name: LOGOUTNOTIFICATION, object: nil)
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate {
    func changeRootViewControllerWithIdentifier(identifier:String!) {
        let desiredViewController:UIViewController = (self.window?.rootViewController?.storyboard?.instantiateViewControllerWithIdentifier(identifier))!;
        
        let snapshot:UIView = (self.window?.snapshotViewAfterScreenUpdates(true))!
        desiredViewController.view.addSubview(snapshot);
        
        self.window?.rootViewController = desiredViewController;
        
        UIView.animateWithDuration(0.3, animations: {() in
            snapshot.layer.opacity = 0;
            snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5);
            }, completion: {
                (value: Bool) in
                snapshot.removeFromSuperview();
        });
    }
}

// Adding extension as class accessor for the app delegate
extension AppDelegate {
    class func sharedAppDelegate() -> AppDelegate? {
        return UIApplication.sharedApplication().delegate as? AppDelegate;
    }
}

extension AppDelegate {
    func logoutNotificationTriggered() {
        //Logout and nil the eventuser
        Requester.sharedInstance.logout()
        
        // Replace the root view controller
        AppDelegate.sharedAppDelegate()?.changeRootViewControllerWithIdentifier("QREMLoginNavigationController")
    }
}

extension Double {
    func roundToPlaces(places: Int)  -> Double {
        let divisor = pow(10.0, Double(places))
        return round(self * divisor) / divisor
    }
}

extension String {
    /**
     Extension
     
     - parameter key:     <#key description#>
     - parameter comment: <#comment description#>
     
     - returns: <#return value description#>
     */
    public static func localize(key: String, comment: String) -> String {
        return NSLocalizedString(key, comment: comment)
    }
    
    
    func toBase64() -> String? {
        let plainData = self.dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData?.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        return base64String
    }
    
    func fromBase64() -> String? {
        let decodedData = NSData(base64EncodedString: self, options: .IgnoreUnknownCharacters)
        let decodedString = NSString(data: decodedData!, encoding: NSUTF8StringEncoding)
        return String(decodedString)
    }
    
}




