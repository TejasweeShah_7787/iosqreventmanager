//
//  EventUser.swift
//  QREventManager
//
//  Created by Hannes Töllborg on 22/02/16.
//  Copyright © 2016 Hannes Töllborg. All rights reserved.
//

import Foundation

class EventUser {
    

    private(set) var eventId:String
    private(set) var password:String?
    
    //computed property
    internal var authorizationCredentials:String? {
        if let pass = self.password {
            return (self.eventId + ":" + pass).toBase64()!
        }
        return nil
    }
    
    //initializer
    init(eventId:String, passwd: String){
        self.eventId = eventId
        self.password = passwd
    }

}