//
//  AppConfig.swift
//  QREventManager
//
//  Created by Hannes Töllborg on 17/02/16.
//  Copyright © 2016 Hannes Töllborg. All rights reserved.
//

import UIKit

// Global application configuration goes here!

let DEVELOPMODE: Bool = false
let NETWORKENABLED: Bool = true

let CCPRIMARY = UIColor(red: 0, green: 90/255, blue: 160/255, alpha: 1)
let CCSECONDARY = UIColor(red: 156/255, green: 178/255, blue: 39/255, alpha: 1)
let CCTERTIARY = UIColor(red: 245/255, green: 126/255, blue: 32/255, alpha: 1)
let CCQUARTERNARY = UIColor(red: 115/255, green: 59/255, blue: 151/255, alpha: 1)
