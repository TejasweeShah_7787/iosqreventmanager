//
//  QREMNetworkConfig.swift
//  QREventManager
//
//  Created by Hannes Töllborg on 17/02/16.
//  Copyright © 2016 Hannes Töllborg. All rights reserved.
//

import Foundation
import Alamofire

// Public network configuration
public struct NetworkConfig {
    static var BASEURL: String {
        if(DEVELOPMODE) {
            return "http://localhost:8080/QR-Backend-web/rest"
        } else {
            return "http://qrevents.proj.cybercomhosting.com:8080/QR-Backend-web/rest"
        }
    }
}