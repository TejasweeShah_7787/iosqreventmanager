//
//  Constants.swift
//  QREventManager
//
//  Created by Hannes Töllborg on 17/02/16.
//  Copyright © 2016 Hannes Töllborg. All rights reserved.
//

import Foundation

// -----------------------------------------------------------------------------
//                                NOTIFICATIONS
// -----------------------------------------------------------------------------
let LOGOUTNOTIFICATION: String = "QREMLogoutNotification"


// -----------------------------------------------------------------------------
//                                ENUMS
// -----------------------------------------------------------------------------
enum STATUS_CODES: Int {
    case OK = 200
    case INCORRECT = 400
    case NOMATCH = 401
    case SIGNED = 208
    case UNKNOWN = 403
    case CONFLICT = 409
}