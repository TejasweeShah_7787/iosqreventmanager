//
//  QREMNetworkHandler.swift
//  QREventManager
//
//  Created by Hannes Töllborg on 17/02/16.
//  Copyright © 2016 Hannes Töllborg. All rights reserved.
//

import Foundation
import Alamofire

class Requester {
    
    // This will pose a stateful behavior....
    //private var _credential: NSURLCredential?
    var eventUser:EventUser?
    
    // set cache to nil, ignore local cache data
    private static let sharedRequestManager: Manager = {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPAdditionalHeaders = Manager.defaultHTTPHeaders
        configuration.URLCache = nil
        //configuration.requestCachePolicy = .ReloadIgnoringLocalCacheData
        
        return Manager(configuration: configuration)
    }()
    
    // -------------------------------------------------------------------------
    //                                  INTERNAL
    // -------------------------------------------------------------------------
    
    
    private func randomGenerator(url: URLStringConvertible) -> URLStringConvertible {
        let randomNumber = arc4random() % 9999;
        return url.URLString + "?cache=" + String(randomNumber)
    }
    
    
    private func _request(method: Alamofire.Method, _ url: URLStringConvertible, parameters:[String:AnyObject]?)
        -> Request? {
            //if let _credential = _credential as NSURLCredential! {
            //var headers = Requester.sharedRequestManager.session.configuration.HTTPAdditionalHeaders
            var headers: [String:String] = [:]
            if let auth = eventUser!.authorizationCredentials {
                headers["Authorization"] = auth
                //Requester.sharedRequestManager.session.configuration.HTTPAdditionalHeaders = headers!
            }
            return Requester.sharedRequestManager.request(method, url, parameters: parameters, headers: headers, encoding: .JSON)
                //.authenticate(usingCredential: _credential)
                .validate(statusCode: 200..<300)
            //}

    }
    
        func logout() {
            if ((eventUser as EventUser!) != nil) {
                eventUser = nil
            }
        }
    
    // -------------------------------------------------------------------------
    //                                  Accessors
    // -------------------------------------------------------------------------
    
    func GetRequest(url: URLStringConvertible) -> Request? {
        return self._request(.GET, url, parameters: [:])
    }
    
    func PostRequest(url: URLStringConvertible, parameters:[String:AnyObject]?) -> Request? {
        return self._request(.POST, url, parameters: parameters)
    }
    
    // -------------------------------------------------------------------------
    //                                  Helpers
    // -------------------------------------------------------------------------
    
    //    func setCredential(credential: NSURLCredential) {
    //        _credential = credential
    //    }
    
    // -------------------------------------------------------------------------
    //                                  Singleton
    // -------------------------------------------------------------------------
    
    // This prevents anyone from trying to instantiate the class
    private init() {
    }
    
    deinit {
    }
    
    // Singleton mapping (ONE LINE, YAY!)
    static let sharedInstance = Requester()
}
